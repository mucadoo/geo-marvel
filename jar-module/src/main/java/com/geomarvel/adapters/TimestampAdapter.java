package com.geomarvel.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class TimestampAdapter extends XmlAdapter<String, Timestamp> {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    @Override
    public Timestamp unmarshal(String value) throws Exception {
        return Timestamp.valueOf(value);
    }

    @Override
    public String marshal(Timestamp value) throws Exception {
        return dateFormat.format(value);
    }
}
