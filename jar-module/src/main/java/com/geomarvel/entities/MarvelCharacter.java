package com.geomarvel.entities;

import lombok.Data;
import org.json.JSONObject;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(name = "marvel")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class MarvelCharacter implements Serializable {
    private static final long serialVersionUID = 1L; // Optional but recommended

    private Integer id;
    private String name;
    private String description;
    private String thumbnailUrl;

    public MarvelCharacter() {
    }

    public MarvelCharacter(JSONObject json) {
        this.id = json.getJSONObject("data").getJSONArray("results").getJSONObject(0).getInt("id");
        this.name = json.getJSONObject("data").getJSONArray("results").getJSONObject(0).getString("name");
        this.description = json.getJSONObject("data").getJSONArray("results").getJSONObject(0).getString("description");
        this.thumbnailUrl = json.getJSONObject("data").getJSONArray("results").getJSONObject(0).getJSONObject("thumbnail").getString("path")
                + "." + json.getJSONObject("data").getJSONArray("results").getJSONObject(0).getJSONObject("thumbnail").getString("extension");
    }

}
