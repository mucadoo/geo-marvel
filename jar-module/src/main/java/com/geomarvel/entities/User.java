package com.geomarvel.entities;

import lombok.Data;
import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "\"USER\"")
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @XmlAttribute
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @XmlElement
    @Column(name = "first_name")
    private String firstName;

    @XmlElement
    @Column(name = "last_name")
    private String lastName;

    @XmlElement
    @Column(name = "username")
    private String username;

    @XmlElement
    @Column(name = "password_hash")
    private String passwordHash;

    @Column(name = "marvel_character_id")
    private int characterId;

}
