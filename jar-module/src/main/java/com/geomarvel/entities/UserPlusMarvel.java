package com.geomarvel.entities;

import lombok.Data;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class UserPlusMarvel implements Serializable {
    private static final long serialVersionUID = 1L; // Optional but recommended

    @XmlElement
    private String firstName;
    @XmlElement
    private String lastName;
    @XmlElement
    private String characterName;
    @XmlElement
    private String characterImageUrl;
    @XmlElement
    private String characterDescription;

    public UserPlusMarvel() {
    }

    public UserPlusMarvel(User user, MarvelCharacter marvelCharacter) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.characterName = marvelCharacter.getName();
        this.characterImageUrl = marvelCharacter.getThumbnailUrl();
        this.characterDescription = marvelCharacter.getDescription();
    }

}
