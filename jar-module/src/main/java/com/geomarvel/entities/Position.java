package com.geomarvel.entities;

import com.geomarvel.adapters.TimestampAdapter;
import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.sql.Timestamp;

@XmlRootElement(name = "position")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "position")
@Data
public class Position implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @XmlElement(name = "timestamp")
    @XmlJavaTypeAdapter(TimestampAdapter.class)
    @Transient
    private Timestamp timestamp;

    @XmlElement(name = "user_id")
    @Column(name = "user_id")
    private int userId;

    @XmlElement(name = "latitude")
    @Column(name = "latitude")
    private double latitude;

    @XmlElement(name = "longitude")
    @Column(name = "longitude")
    private double longitude;

    public Position() {
    }

}
