package com.geomarvel.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Proxy {
    private final String host;
    private final int port;
    private final String user;
    private final String pass;
}
