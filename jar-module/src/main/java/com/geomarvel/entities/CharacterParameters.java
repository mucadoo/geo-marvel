package com.geomarvel.entities;

import lombok.Data;

@Data
public class CharacterParameters {
    private Integer id;
    private String name;
    private String nameStartsWith;
    private Integer limit;
}
