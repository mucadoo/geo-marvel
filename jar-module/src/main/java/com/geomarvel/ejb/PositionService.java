package com.geomarvel.ejb;

import com.geomarvel.entities.Position;
import java.util.List;
import javax.ejb.Remote;
import java.io.IOException;

@Remote
public interface PositionService {

    void save(Position position) throws IOException;

    List<Position> listAll() throws IOException;

    List<Position> findByUserId(int userId) throws IOException;
}
