package com.geomarvel.ejb;

import com.geomarvel.entities.User;
import java.util.Collection;
import java.util.List;
import javax.ejb.Remote;
import java.io.IOException;

@Remote
public interface UserService {

    User createUser(User user) throws IOException;

    List<User> list() throws IOException;

    User findUserById(int id) throws IOException;

    List<User> findUserByName(String name) throws IOException;

    void removeUser(int id) throws IOException;

    void updateUser(User user) throws IOException;

    boolean authenticate(String username, String password) throws IOException;

    User changePassword(String username, String oldPassword, String newPassword) throws IOException;

    String getHash(String password) throws IOException;
}
