package com.geomarvel.ejb;

import com.geomarvel.entities.CharacterParameters;
import com.geomarvel.entities.MarvelCharacter;
import java.io.IOException;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface MarvelCharacterService {

    List<MarvelCharacter> getCharacters(CharacterParameters characterParameters) throws IOException;

    MarvelCharacter getCharacter(int id) throws IOException;
}
