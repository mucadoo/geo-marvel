package com.geomarvel.utils;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;
import java.util.logging.Logger;

public class Logging {

    private static final Logger LOGGER = Logger.getLogger(Logging.class.getName());

    public static void logUserRoles(SecurityContext sc) {
        boolean isReader = sc.isUserInRole("reader");
        boolean isAdministrator = sc.isUserInRole("administrator");
        Principal p = sc.getUserPrincipal();
        if (p != null) {
            LOGGER.info("Principal " + p.getName() + " belongs to reader, administrator roles: " + isReader + ", " + isAdministrator);
        } else {
            LOGGER.info("No principal found. User roles: reader - " + isReader + ", administrator - " + isAdministrator);
        }
    }
}
