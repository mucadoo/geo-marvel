package com.geomarvel.utils;

public class JndiConstants {
    public static final String USER_SERVICE = "java:global/ear-module-1.0-SNAPSHOT/ejb-module/UserServiceImpl";
    public static final String MARVEL_CHARACTER_SERVICE = "java:global/ear-module-1.0-SNAPSHOT/ejb-module/MarvelCharacterServiceImpl";
    public static final String POSITION_SERVICE = "java:global/ear-module-1.0-SNAPSHOT/ejb-module/PositionServiceImpl";
}
