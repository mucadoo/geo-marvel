package com.geomarvel.utils;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceLookup {

    private static final Logger LOGGER = Logger.getLogger(ServiceLookup.class.getName());

    public static <T> T lookupService(Class<T> serviceClass, String jndiName) {
        try {
            InitialContext ctx = new InitialContext();
            return (T) ctx.lookup(jndiName);
        } catch (NamingException ex) {
            LOGGER.log(Level.SEVERE, "Service lookup failed for: " + jndiName, ex);
            throw new RuntimeException("Service lookup failed for: " + jndiName, ex);
        }
    }
}
