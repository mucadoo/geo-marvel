package com.geomarvel.utils;

import com.geomarvel.entities.CharacterParameters;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class URLFactory {

    private static final String BASE_URL = "/v1/public/";
    private static final String CHARACTERS_URL = BASE_URL + "characters";

    private final String publicKey;
    private final String privateKey;

    public URLFactory(String privateKey, String publicKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public String getCharactersURL(CharacterParameters params) throws UnsupportedEncodingException {
        long timeStamp = System.currentTimeMillis();
        StringBuilder url = new StringBuilder(CHARACTERS_URL);

        if (params.getId() != null) {
            url.append("/").append(params.getId()).append("?");
        } else {
            url.append("?");
            url.append(params.getName() == null ? "" : "name=" + URLEncoder.encode(params.getName(), "UTF-8") + "&");
            url.append(params.getNameStartsWith() == null ? "" : "nameStartsWith=" + URLEncoder.encode(params.getNameStartsWith(), "UTF-8") + "&");
            url.append(params.getLimit() == null ? "" : "limit=" + params.getLimit() + "&");
        }
        url.append("apikey=").append(publicKey)
                .append("&hash=").append(createHash(timeStamp))
                .append("&ts=").append(timeStamp);

        return url.toString();
    }

    private String createHash(long timeStamp) {
        String stringToHash = timeStamp + privateKey + publicKey;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hashBytes = md.digest(stringToHash.getBytes());
            return byteToHex(hashBytes);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String byteToHex(byte[] bytes) {
        Formatter formatter = new Formatter();
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }
}
