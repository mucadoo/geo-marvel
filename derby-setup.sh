#!/bin/sh

LOG_FILE="/app/initialization.log"
DB_INIT_FLAG="/app/db_initialized"
DERBY_PROPERTIES="/dbs/derby.properties"

# Function to check if Derby server is ready
check_derby_ready() {
  while ! nc -z localhost 1527; do
    echo "$(date): Waiting for Derby to start..." >> ${LOG_FILE}
    sleep 2
  done
}

# Check if the initialization flag file exists
if [ ! -f ${DB_INIT_FLAG} ]; then
  echo "$(date): Initializing database..." >> ${LOG_FILE}
  java -Dderby.stream.error.field=java.lang.System.out org.apache.derby.drda.NetworkServerControl start -h 0.0.0.0 &
  check_derby_ready  # Wait for Derby to be ready

  # Create derby.properties file with authentication settings
  echo "$(date): Creating derby.properties file..." >> ${LOG_FILE}
  echo "derby.authentication.provider=BUILTIN" > ${DERBY_PROPERTIES}
  echo "derby.user.admin=admin_password" >> ${DERBY_PROPERTIES}

  # Run the first part of the init.sql script
  echo "CONNECT 'jdbc:derby://localhost:1527/${DB_NAME};create=true;user=admin;password=admin_password';" > /app/create_and_init.sql
  echo "CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY('derby.user.${DB_USER}', '${DB_PASSWORD}');" >> /app/create_and_init.sql
  echo "CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY('derby.connection.requireAuthentication', 'true');" >> /app/create_and_init.sql
  echo "CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY('derby.authentication.provider', 'BUILTIN');" >> /app/create_and_init.sql
  echo "CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY('derby.database.sqlAuthorization', 'true');" >> /app/create_and_init.sql
  java -cp ${DERBY_LIB}/derbytools.jar org.apache.derby.tools.ij /app/create_and_init.sql

  echo "$(date): Database initialized." >> ${LOG_FILE}
  touch ${DB_INIT_FLAG}  # Create a flag file to indicate initialization is done
else
  echo "$(date): Database already initialized." >> ${LOG_FILE}
  java -Dderby.stream.error.field=java.lang.System.out org.apache.derby.drda.NetworkServerControl start -h 0.0.0.0
fi

# Keep the server running
tail -f /dev/null
