package com.geomarvel.interceptors;

import javax.annotation.Resource;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

public class LogInterceptor {

    @Inject
    @JMSConnectionFactory("java:/RemoteJmsXA")
    private JMSContext jmsContext;

    @Resource(lookup = "java:global/remoteContext/LogQueue")
    private Queue logQueue;

    @AroundInvoke
    public Object log(InvocationContext ctx) throws Exception {
        String logMessage = "Entering method: " + ctx.getMethod().getName();
        jmsContext.createProducer().send(logQueue, logMessage);
        System.out.println(logMessage);

        Object result = ctx.proceed();

        logMessage = "Exiting method: " + ctx.getMethod().getName();
        jmsContext.createProducer().send(logQueue, logMessage);
        System.out.println(logMessage);

        return result;
    }
}
