package com.geomarvel.ejb;

import com.geomarvel.entities.User;
import com.geomarvel.interceptors.LogInterceptor;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Interceptors(LogInterceptor.class)
public class UserServiceImpl implements UserService {

    @PersistenceContext(unitName = "DerbyPU")
    private EntityManager em;

    @Override
    public User createUser(User user) {
        em.persist(user);
        em.flush();
        em.refresh(user);
        return user;
    }

    @Override
    public List<User> list() {
        Query query = em.createQuery("FROM \"USER\" u");
        return query.getResultList();
    }

    @Override
    public User findUserById(int id) {
        return em.find(User.class, id);
    }

    @Override
    public List<User> findUserByName(String name) {
        Query query = em.createQuery("SELECT u FROM \"USER\" u WHERE u.firstName = :name");
        query.setParameter("name", name);
        return query.getResultList();
    }

    @Override
    public void removeUser(int id) {
        User user = em.find(User.class, id);
        if (user != null) {
            em.remove(user);
        }
    }

    @Override
    public void updateUser(User user) {
        User existingUser = em.find(User.class, user.getId());
        if (existingUser != null) {
            existingUser.setFirstName(user.getFirstName());
            existingUser.setLastName(user.getLastName());
            existingUser.setUsername(user.getUsername());
            em.merge(existingUser);
        }
    }

    @Override
    public boolean authenticate(String username, String password) {
        Query query = em.createQuery("FROM \"USER\" u WHERE u.username = :username");
        query.setParameter("username", username);
        List<User> list = query.getResultList();
        if (list.size() != 1) {
            return false;
        }
        User user = list.get(0);
        try {
            return username.equals(user.getUsername()) && validatePassword(password, user.getPasswordHash());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public User changePassword(String username, String oldPassword, String newPassword) {
        Query query = em.createQuery("FROM \"USER\" u WHERE u.username = :username");
        query.setParameter("username", username);
        List<User> list = query.getResultList();
        if (list.size() != 1) {
            return null;
        }
        User user = list.get(0);
        try {
            if (username.equals(user.getUsername()) && validatePassword(oldPassword, user.getPasswordHash())) {
                user.setPasswordHash(generateStrongPasswordHash(newPassword));
                em.persist(user);
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean validatePassword(String candidatePassword, String storedHash) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String[] parts = storedHash.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);
        PBEKeySpec spec = new PBEKeySpec(candidatePassword.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] candidateHash = skf.generateSecret(spec).getEncoded();
        int diff = hash.length ^ candidateHash.length;
        for (int i = 0; i < hash.length && i < candidateHash.length; i++) {
            diff |= hash[i] ^ candidateHash[i];
        }
        return diff == 0;
    }

    @Override
    public String getHash(String password) {
        try {
            return generateStrongPasswordHash(password);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private String generateStrongPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        int iterations = 1000;
        char[] chars = password.toCharArray();
        byte[] salt = getSalt().getBytes();
        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }

    private String getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt.toString();
    }

    private String toHex(byte[] array) throws NoSuchAlgorithmException {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    private byte[] fromHex(String hex) throws NoSuchAlgorithmException {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
}
