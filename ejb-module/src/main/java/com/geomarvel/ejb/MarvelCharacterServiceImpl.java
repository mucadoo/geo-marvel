package com.geomarvel.ejb;

import com.geomarvel.entities.CharacterParameters;
import com.geomarvel.entities.MarvelCharacter;
import com.geomarvel.exceptions.MarvelRestException;
import com.geomarvel.utils.URLFactory;
import org.json.JSONObject;

import javax.ejb.Stateless;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class MarvelCharacterServiceImpl implements MarvelCharacterService {

    private static final String HOST = "gateway.marvel.com";
    private final String privateApiKey = System.getenv("MARVEL_API_KEY_PRIVATE");
    private final String publicApiKey = System.getenv("MARVEL_API_KEY_PUBLIC");
    private final URLFactory urlFactory = new URLFactory(privateApiKey, publicApiKey);
    private final com.geomarvel.entities.Proxy proxy = null;
    private static final Logger LOGGER = Logger.getLogger(MarvelCharacterServiceImpl.class.getName());

    @Override
    public List<MarvelCharacter> getCharacters(CharacterParameters characterParameters) throws IOException {
        final String result = getURL(urlFactory.getCharactersURL(characterParameters));
        return resultToMarvelCharacter(result);
    }

    @Override
    public MarvelCharacter getCharacter(int id) throws IOException {
        CharacterParameters characterParameters = new CharacterParameters();
        characterParameters.setId(id);
        return getCharacters(characterParameters).get(0);
    }

    private List<MarvelCharacter> resultToMarvelCharacter(String result) {
        List<MarvelCharacter> list = new ArrayList<>();
        list.add(new MarvelCharacter(new JSONObject(result)));
        return list;
    }

    private String getURL(String urlString) throws IOException {
        URL url = new URL("https://" + HOST + urlString);
        HttpURLConnection conn;

        if (proxy == null) {
            conn = (HttpURLConnection) url.openConnection();
        } else {
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(this.proxy.getHost(), this.proxy.getPort()));
            conn = (HttpURLConnection) url.openConnection(proxy);

            String encoded = Base64.getEncoder().encodeToString((this.proxy.getUser() + ":" + this.proxy.getPass()).getBytes());
            conn.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
        }

        conn.setRequestMethod("GET");

        int responseCode = conn.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            throw new MarvelRestException(conn);
        }

        try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            return content.toString();
        } finally {
            conn.disconnect();
        }
    }
}
