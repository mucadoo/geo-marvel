package com.geomarvel.ejb;

import com.geomarvel.entities.Position;
import com.geomarvel.interceptors.LogInterceptor;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
@Interceptors(LogInterceptor.class)
public class PositionServiceImpl implements PositionService {

    @PersistenceContext(unitName = "DerbyPU")
    private EntityManager em;

    @Override
    public void save(Position position) {
        em.persist(position);
    }

    @Override
    public List<Position> listAll() {
        Query query = em.createQuery("FROM Position p");
        return query.getResultList();
    }

    @Override
    public List<Position> findByUserId(int userId) {
        Query query = em.createQuery("FROM Position p WHERE p.userId = :userId");
        query.setParameter("userId", userId);
        return query.getResultList();
    }
}
