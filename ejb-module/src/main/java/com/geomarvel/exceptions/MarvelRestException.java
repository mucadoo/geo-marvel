package com.geomarvel.exceptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class MarvelRestException extends RuntimeException {

    private final int responseCode;
    private final String responseMessage;

    public MarvelRestException(HttpURLConnection connection) throws IOException {
        super(getResponseMessage(connection));
        this.responseCode = connection.getResponseCode();
        this.responseMessage = getResponseMessage(connection);
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    private static String getResponseMessage(HttpURLConnection connection) throws IOException {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getErrorStream()))) {
            StringBuilder content = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            return content.toString();
        }
    }
}
