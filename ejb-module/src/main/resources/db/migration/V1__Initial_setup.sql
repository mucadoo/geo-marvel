-- Create Users Table
CREATE TABLE "USER"
(
    ID                  INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    USERNAME            VARCHAR(255) NOT NULL UNIQUE,
    PASSWORD_HASH       VARCHAR(255) NOT NULL,
    FIRST_NAME          VARCHAR(255),
    LAST_NAME           VARCHAR(255),
    MARVEL_CHARACTER_ID INT
);

-- Insert Initial Users
INSERT INTO "USER" (USERNAME, PASSWORD_HASH, FIRST_NAME, LAST_NAME, MARVEL_CHARACTER_ID)
VALUES ('admin', 'hashed_password', 'Bruce', 'Banner', 1009351),         -- Hulk
       ('spider_man', 'hashed_password_spidey', 'Peter', 'Parker', 1009610), -- Spider-Man
       ('iron_man', 'hashed_password_tony', 'Tony', 'Stark', 1009368),      -- Iron Man
       ('captain_america', 'hashed_password_steve', 'Steve', 'Rogers', 1009220), -- Captain America
       ('thor', 'hashed_password_thor', 'Thor', 'Odinson', 1009664);       -- Thor

-- Create Positions Table
CREATE TABLE POSITION
(
    ID      INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    LATITUDE DOUBLE NOT NULL,
    LONGITUDE DOUBLE NOT NULL,
    USER_ID INT,
    FOREIGN KEY (USER_ID) REFERENCES "USER" (ID)
);

-- Insert Initial Positions
INSERT INTO POSITION (LATITUDE, LONGITUDE, USER_ID)
VALUES (40.712776, -74.005974, 1),  -- New York City, USA (Hulk)
       (-23.550520, -46.633308, 2), -- São Paulo, Brazil (Spider-Man)
       (48.856613, 2.352222, 3),    -- Paris, France (Iron Man)
       (30.044420, 31.235712, 4),   -- Cairo, Egypt (Captain America)
       (35.689487, 139.691711, 5);  -- Tokyo, Japan (Thor)
