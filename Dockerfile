# Stage 1: Build the project using Maven
FROM maven:3.8.8-eclipse-temurin-8 AS builder

# Set the working directory inside the container
WORKDIR /app

# Copy the entire project to the container
COPY . .

# Package the EAR file
RUN mvn clean package -X

# Stage 2: Deploy the EAR file to WildFly
FROM jboss/wildfly:25.0.0.Final

# Set the working directory
WORKDIR /opt/jboss/wildfly

# Copy the EAR file from the builder stage to the deployment folder
COPY --from=builder /app/ear-module/target/ear-module-1.0-SNAPSHOT.ear standalone/deployments/

# Download and set up Derby client driver
RUN curl -O https://downloads.apache.org//db/derby/db-derby-10.14.2.0/db-derby-10.14.2.0-bin.zip \
    && unzip db-derby-10.14.2.0-bin.zip -d /opt/jboss/wildfly/derby \
    && rm db-derby-10.14.2.0-bin.zip

# Install Flyway in /tmp/flyway directory
RUN mkdir -p /tmp/flyway && \
    curl -L https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/10.17.0/flyway-commandline-10.17.0-linux-x64.tar.gz \
    | tar -xz -C /tmp/flyway --strip-components=1

# Create the drivers directory and download the necessary Derby JAR files using curl
RUN mkdir -p /tmp/flyway/drivers && \
    curl -o /tmp/flyway/drivers/derbyclient.jar https://repo1.maven.org/maven2/org/apache/derby/derbyclient/10.16.1.1/derbyclient-10.16.1.1.jar && \
    curl -o /tmp/flyway/drivers/derby.jar https://repo1.maven.org/maven2/org/apache/derby/derby/10.16.1.1/derby-10.16.1.1.jar && \
    curl -o /tmp/flyway/drivers/derbyshared.jar https://repo1.maven.org/maven2/org/apache/derby/derbyshared/10.16.1.1/derbyshared-10.16.1.1.jar && \
    curl -o /tmp/flyway/drivers/derbytools.jar https://repo1.maven.org/maven2/org/apache/derby/derbytools/10.16.1.1/derbytools-10.16.1.1.jar

# Ensure the PATH is updated
ENV PATH="/tmp/flyway:${PATH}"

# Set the Flyway driver environment variable
ENV FLYWAY_DRIVER=org.apache.derby.jdbc.ClientDriver

# Copy migration scripts
COPY ejb-module/src/main/resources/db/migration /opt/jboss/wildfly/db/migration

# Copy the CLI configuration file to the temporary folder
COPY wildfly-setup.cli /tmp/

# Run the CLI configuration
RUN /opt/jboss/wildfly/bin/jboss-cli.sh --file=/tmp/wildfly-setup.cli

# Clean up the standalone_xml_history/current directory
RUN rm -rf /opt/jboss/wildfly/standalone/configuration/standalone_xml_history/current/*

# Expose WildFly ports
EXPOSE 8080 9990

# Start WildFly in standalone-full-ha mode, binding to all interfaces
CMD ["sh", "-c", "flyway -url=jdbc:derby://derby:1527/${DB_NAME} -user=${DB_USER} -password=${DB_PASSWORD} -locations=filesystem:/opt/jboss/wildfly/db/migration migrate && bin/standalone.sh -c standalone-full-ha.xml -b 0.0.0.0 -bmanagement 0.0.0.0"]
