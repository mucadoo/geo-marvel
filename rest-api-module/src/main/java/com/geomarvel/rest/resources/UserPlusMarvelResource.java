package com.geomarvel.rest.resources;

import com.geomarvel.ejb.MarvelCharacterService;
import com.geomarvel.ejb.UserService;
import com.geomarvel.entities.MarvelCharacter;
import com.geomarvel.entities.User;
import com.geomarvel.entities.UserPlusMarvel;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.util.logging.Logger;

import static com.geomarvel.utils.Logging.logUserRoles;

@Path("/userplusmarvel")
public class UserPlusMarvelResource {

    private static final Logger LOGGER = Logger.getLogger(UserPlusMarvelResource.class.getName());

    public UserPlusMarvelResource() {
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserPlusMarvel findUserPlusMarvelById(@Context SecurityContext sc, @PathParam("id") final int id) throws IOException {
        logUserRoles(sc);
        UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);
        MarvelCharacterService characterService = ServiceLookup.lookupService(MarvelCharacterService.class, JndiConstants.MARVEL_CHARACTER_SERVICE);
        LOGGER.info("Finding information for user: " + id);
        User user = userService.findUserById(id);
        if (user == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        MarvelCharacter character = characterService.getCharacter(user.getCharacterId());
        return new UserPlusMarvel(user, character);
    }
}
