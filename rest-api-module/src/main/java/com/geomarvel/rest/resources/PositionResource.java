package com.geomarvel.rest.resources;

import com.geomarvel.ejb.PositionService;
import com.geomarvel.entities.Position;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.util.logging.Logger;

import static com.geomarvel.utils.Logging.logUserRoles;

@Path("/position")
public class PositionResource {

    private static final Logger LOGGER = Logger.getLogger(PositionResource.class.getName());

    public PositionResource() {
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Position findPositionByUserId(@Context SecurityContext sc, @PathParam("id") final int id) throws IOException {
        logUserRoles(sc);
        PositionService positionService = ServiceLookup.lookupService(PositionService.class, JndiConstants.POSITION_SERVICE);
        LOGGER.info("Finding positions for user: " + id);
        Position position = positionService.findByUserId(id).get(0);
        if (position == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return position;
    }
}
