package com.geomarvel.rest.resources;

import com.geomarvel.ejb.MarvelCharacterService;
import com.geomarvel.entities.MarvelCharacter;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.util.logging.Logger;

import static com.geomarvel.utils.Logging.logUserRoles;

@Path("/marvel")
public class MarvelCharacterResource {

    private static final Logger LOGGER = Logger.getLogger(MarvelCharacterResource.class.getName());

    public MarvelCharacterResource() {
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public MarvelCharacter findCharacterById(@Context SecurityContext sc, @PathParam("id") final int id) throws IOException {
        logUserRoles(sc);
        MarvelCharacterService characterService = ServiceLookup.lookupService(MarvelCharacterService.class, JndiConstants.MARVEL_CHARACTER_SERVICE);
        LOGGER.info("Finding character: " + id);
        MarvelCharacter character = characterService.getCharacter(id);
        if (character == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return character;
    }
}
