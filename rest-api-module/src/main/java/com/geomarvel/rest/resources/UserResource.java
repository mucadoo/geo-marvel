package com.geomarvel.rest.resources;

import com.geomarvel.ejb.UserService;
import com.geomarvel.entities.User;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.xml.bind.*;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.util.logging.Logger;

import static com.geomarvel.utils.Logging.logUserRoles;

@Path("/user")
public class UserResource {

    private static final Logger LOGGER = Logger.getLogger(UserResource.class.getName());

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public User findUserById(@Context SecurityContext sc, @PathParam("id") final int id) throws IOException {
        logUserRoles(sc);
        UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);
        LOGGER.info("Finding user: " + id);
        User user = userService.findUserById(id);
        if (user == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return user;
    }

    @RolesAllowed({"administrator"})
    @POST
    @Path("/new")
    @Consumes(MediaType.APPLICATION_XML)
    public Response createUser(String userXml) {
        UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);
        int id = 0;
        try {
            JAXBContext jc = JAXBContext.newInstance(User.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            StringReader reader = new StringReader(userXml);
            User user = (User) unmarshaller.unmarshal(reader);
            User newUser = userService.createUser(user);
            id = newUser.getId();
        } catch (JAXBException | IOException ex) {
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.created(URI.create("/user/" + id)).build();
    }

    @RolesAllowed({"administrator"})
    @DELETE
    @Path("/{id}")
    public Response removeUserById(@PathParam("id") final int id) throws IOException {
        UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);
        userService.removeUser(id);
        return Response.status(Response.Status.GONE).build();
    }
}
