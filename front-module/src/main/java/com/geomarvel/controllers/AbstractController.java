package com.geomarvel.controllers;

import javax.servlet.http.HttpServletRequest;

public abstract class AbstractController implements Controller {

    private HttpServletRequest request;
    protected String returnPage;

    @Override
    public void init(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setReturnPage(String page) {
        this.returnPage = page;
    }

    @Override
    public String getReturnPage() {
        return returnPage;
    }

    protected HttpServletRequest getRequest() {
        return request;
    }
}
