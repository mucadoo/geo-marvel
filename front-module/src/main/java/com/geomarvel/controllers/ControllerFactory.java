package com.geomarvel.controllers;

public class ControllerFactory {

    public static Controller getControllerByClass(Class<? extends Controller> actionClass) {
        try {
            return actionClass.getDeclaredConstructor().newInstance();
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Controller getControllerByFullClassName(String className) {
        try {
            String name = "com.geomarvel.controllers.impl." + className + "Controller";
            Class<? extends Controller> actionClass = (Class<? extends Controller>) Class.forName(name);
            return getControllerByClass(actionClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
