package com.geomarvel.controllers;

import javax.servlet.http.HttpServletRequest;

public interface Controller {

    void execute();

    void init(HttpServletRequest request);

    void setReturnPage(String page);

    String getReturnPage();
}
