package com.geomarvel.controllers.impl;

import com.geomarvel.ejb.UserService;
import com.geomarvel.entities.User;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;

import java.util.logging.Level;
import java.util.logging.Logger;
import com.geomarvel.controllers.AbstractController;

public class SearchByIdController extends AbstractController {

    @Override
    public void execute() {
        try {
            Logger.getLogger(SearchByIdController.class.getName()).log(Level.INFO, null, "Searching user by ID");
            String id = getRequest().getParameter("id");
            UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);
            User user = userService.findUserById(Integer.parseInt(id));
            setReturnPage("/showUser.jsp");
            getRequest().setAttribute("user", user);
        } catch (Exception ex) {
            Logger.getLogger(SearchByIdController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
