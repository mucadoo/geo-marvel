package com.geomarvel.controllers.impl;

import com.geomarvel.ejb.UserService;
import com.geomarvel.entities.User;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;
import com.geomarvel.controllers.AbstractController;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RemoveController extends AbstractController {

    @Override
    public void execute() {
        try {
            Logger.getLogger(RemoveController.class.getName()).log(Level.INFO, null, "Removing user");
            String id = getRequest().getParameter("id");
            UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);
            userService.removeUser(Integer.parseInt(id));
            List<User> users = userService.list();
            setReturnPage("/listUsers.jsp");
            getRequest().setAttribute("users", users);
        } catch (Exception ex) {
            Logger.getLogger(RemoveController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
