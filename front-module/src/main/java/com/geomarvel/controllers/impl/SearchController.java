package com.geomarvel.controllers.impl;

import com.geomarvel.ejb.UserService;
import com.geomarvel.entities.User;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.geomarvel.controllers.AbstractController;

public class SearchController extends AbstractController {

    @Override
    public void execute() {
        try {
            Logger.getLogger(SearchController.class.getName()).log(Level.INFO, null, "Searching users");
            String name = getRequest().getParameter("name");
            UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);
            List<User> users = userService.findUserByName(name);
            setReturnPage("/listUsers.jsp");
            getRequest().setAttribute("users", users);
        } catch (Exception ex) {
            Logger.getLogger(SearchController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
