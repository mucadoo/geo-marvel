package com.geomarvel.controllers.impl;

import com.geomarvel.ejb.UserService;
import com.geomarvel.entities.User;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;
import com.geomarvel.controllers.AbstractController;

import java.util.logging.Level;
import java.util.logging.Logger;

public class NewController extends AbstractController {

    @Override
    public void execute() {
        try {
            Logger.getLogger(NewController.class.getName()).log(Level.INFO, null, "Creating new user");
            String firstName = getRequest().getParameter("firstName");
            String lastName = getRequest().getParameter("lastName");
            String username = getRequest().getParameter("username");
            String password = getRequest().getParameter("password");
            UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);
            User user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setUsername(username);
            user.setPasswordHash(userService.getHash(password));
            user = userService.createUser(user);
            setReturnPage("/showUser.jsp");
            getRequest().setAttribute("user", user);
        } catch (Exception ex) {
            Logger.getLogger(NewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
