package com.geomarvel.controllers.impl;

import com.geomarvel.ejb.UserService;
import com.geomarvel.entities.User;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.geomarvel.controllers.AbstractController;

public class UpdateController extends AbstractController {

    @Override
    public void execute() {
        try {
            Logger.getLogger(UpdateController.class.getName()).log(Level.INFO, null, "Updating user");
            UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);
            String id = getRequest().getParameter("id");
            String firstName = getRequest().getParameter("firstName");
            String lastName = getRequest().getParameter("lastName");
            String username = getRequest().getParameter("username");
            User user = userService.findUserById(Integer.parseInt(id));
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setUsername(username);
            userService.updateUser(user);
            setReturnPage("/showUser.jsp");
            getRequest().setAttribute("user", user);
        } catch (Exception ex) {
            Logger.getLogger(UpdateController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
