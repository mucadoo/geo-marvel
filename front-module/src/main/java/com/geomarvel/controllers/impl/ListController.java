package com.geomarvel.controllers.impl;

import com.geomarvel.ejb.UserService;
import com.geomarvel.entities.User;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;
import com.geomarvel.controllers.AbstractController;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListController extends AbstractController {

    @Override
    public void execute() {
        try {
            Logger.getLogger(ListController.class.getName()).log(Level.INFO, null, "Listing users");
            UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);
            List<User> users = userService.list();
            setReturnPage("/listUsers.jsp");
            getRequest().setAttribute("users", users);
        } catch (Exception ex) {
            Logger.getLogger(ListController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
