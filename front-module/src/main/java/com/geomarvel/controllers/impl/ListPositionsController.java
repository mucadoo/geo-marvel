package com.geomarvel.controllers.impl;

import com.geomarvel.ejb.PositionService;
import com.geomarvel.entities.Position;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;
import com.geomarvel.controllers.AbstractController;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListPositionsController extends AbstractController {

    @Override
    public void execute() {
        try {
            Logger.getLogger(ListPositionsController.class.getName()).log(Level.INFO, null, "Listing positions");
            PositionService positionService = ServiceLookup.lookupService(PositionService.class, JndiConstants.POSITION_SERVICE);
            List<Position> positions = positionService.listAll();
            setReturnPage("/map.jsp");
            getRequest().setAttribute("positions", positions);
        } catch (Exception ex) {
            Logger.getLogger(ListPositionsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
