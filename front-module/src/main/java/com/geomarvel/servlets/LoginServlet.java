package com.geomarvel.servlets;

import com.geomarvel.ejb.UserService;
import com.geomarvel.utils.JndiConstants;
import com.geomarvel.utils.ServiceLookup;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRequest = request.getParameter("login");
        String passwordRequest = request.getParameter("senha");
        UserService userService = ServiceLookup.lookupService(UserService.class, JndiConstants.USER_SERVICE);

        if (userService.authenticate(userRequest, passwordRequest)) {
            HttpSession session = request.getSession();
            session.setAttribute("user", userRequest);
            session.setMaxInactiveInterval(30 * 60);
            Cookie userName = new Cookie("user", userRequest);
            userName.setMaxAge(30 * 60);
            response.addCookie(userName);
            response.sendRedirect("/AppWEB/restricted/successLogin.jsp");
        } else {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.html");
            try (PrintWriter out = response.getWriter()) {
                out.println("<font color=red>Incorrect username or password.</font>");
            }
            rd.include(request, response);
        }
    }
}
