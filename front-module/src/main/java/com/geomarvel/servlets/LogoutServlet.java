package com.geomarvel.servlets;

import javax.servlet.http.*;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("JSESSIONID".equals(cookie.getName())) {
                    System.out.println("JSESSIONID=" + cookie.getValue());
                    break;
                }
            }
        }

        HttpSession session = request.getSession(false);
        System.out.println("User=" + (session != null ? session.getAttribute("user") : "null"));
        if (session != null) {
            session.invalidate();
        }
        response.sendRedirect("login.html");
    }
}
