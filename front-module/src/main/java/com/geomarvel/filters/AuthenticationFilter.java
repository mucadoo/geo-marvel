package com.geomarvel.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Logger;

public class AuthenticationFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(AuthenticationFilter.class.getName());
    private ServletContext context;

    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
        LOGGER.info("Authentication Filter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String uri = req.getRequestURI();
        String controlParam = req.getParameter("control");
        LOGGER.info("Requested Resource::" + uri);

        HttpSession session = req.getSession(false);
        if (session == null && !(uri.endsWith("html") || uri.endsWith("LoginServlet") || (uri.contains("FrontControllerServlet") && "ListPositions".equals(controlParam)))) {
            LOGGER.warning("Unauthorized access request");
            res.sendRedirect("login.html");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        // Close any resources here
    }
}
