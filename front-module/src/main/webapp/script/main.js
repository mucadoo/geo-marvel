function init() {
    var myMap = new ol.Map({
        target: 'MyMap',
        renderer: 'canvas',
        view: new ol.View({
            projection: 'EPSG:900913',
            center: [0, 0],
            zoom: 2
        })
    });
    var openStreetMapLayer = new ol.layer.Tile({
        source: new ol.source.OSM()
    });
    myMap.addLayer(openStreetMapLayer);
    return myMap;
}

function addPoint(myMap, lat, lon, userId) {
    var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.fromLonLat([lon, lat])), // Longitude, Latitude
        name: 'place',
        id: userId
    });

    var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            opacity: 0.75,
            src: 'media/map-pin.png',
            scale: window.devicePixelRatio/4
        }))
    });

    iconFeature.setStyle(iconStyle);
    var vectorSource = new ol.source.Vector({
        features: [iconFeature]
    });
    var vectorLayer = new ol.layer.Vector({
        source: vectorSource
    });
    myMap.addLayer(vectorLayer);
}

function finalize(myMap) {
    var element = document.getElementById('popup');
    var popup = new ol.Overlay({
        element: element,
        positioning: 'bottom-center',
        stopEvent: false
    });
    myMap.addOverlay(popup);
    myMap.on('click', function (evt) {
        popup.setPosition(undefined);
        $(element).popover('destroy');
        var feature = myMap.forEachFeatureAtPixel(evt.pixel,
            function (feature, layer) {
                return feature;
            });
        if (feature) {
            var userId = feature.get('id');
            var urlString = 'http://localhost:8080/rest/api/userplusmarvel/';
            urlString = urlString.concat(userId);
            $.ajax({
                url: urlString,
                data: {
                    format: 'json'
                },
                success: function (data) {
                    $(element).popover({
                        'placement': 'top',
                        'html': true,
                        'content': "<p><strong>" + data.firstName + " " + data.lastName + " - " + data.characterName + "</strong></p>\
                            <img class='img-thumbnail img-responsive pull-left' src='" + data.characterImageUrl + "'>\
                            <p><strong>Description:</strong><small> " + data.characterDescription + "</small></p>"
                    });
                    popup.setPosition(evt.coordinate);
                    $(element).popover('show');
                },
                error: function (e) {
                    console.log(e.message);
                },
                type: 'GET'
            });
        } else {
            $(element).popover('destroy');
        }
    });
    var target = myMap.getTarget();
    var jTarget = typeof target === "string" ? $("#" + target) : $(target);
    $(myMap.getViewport()).on('mousemove', function (e) {
        var pixel = myMap.getEventPixel(e.originalEvent);
        var hit = myMap.forEachFeatureAtPixel(pixel, function (feature, layer) {
            return true;
        });
        if (hit) {
            jTarget.css("cursor", "pointer");
        } else {
            jTarget.css("cursor", "");
        }
    });
}
