<%@ page import="com.geomarvel.entities.Position" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%
    List<Position> positions = (List<Position>) request.getAttribute("positions");
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>GeoMarvel</title>
    <link rel="icon" href="media/favicon.png" type="image/png">
    <link rel="stylesheet" href="style/bootstrap.min.css">
    <link rel="stylesheet" href="style/ol.css" type="text/css">
    <link rel="stylesheet" href="style/main.css" type="text/css">
    <script src="script/jquery-1.11.3.min.js"></script>
    <script src="script/bootstrap.min.js"></script>
    <script src="script/ol.js"></script>
    <script src="script/main.js"></script>
    <script type="text/javascript">
        $(function () {
            var myMap = init();

            <% for (Position p : positions) { %>
            addPoint(myMap, <%= p.getLatitude() %>, <%= p.getLongitude() %>, <%= p.getUserId() %>);
            <% } %>

            finalize(myMap);
        });
    </script>
    <style> html, body { height: 100%; margin: 0; padding: 0; } .map { height: 100%; width: 100%; } </style>
</head>
<body style="height: 100%">
    <div id="MyMap" class="map">
        <div id="popup"></div>
    </div>
</body>
</html>
