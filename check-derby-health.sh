#!/bin/sh

DEBUG_LOG="/app/debug_health_check.log"

# Start of the health check script
echo "$(date): Health check script started." >> ${DEBUG_LOG}

# Function to check if Derby server is listening on the port
check_port() {
  echo "$(date): Checking if port 1527 is open..." >> ${DEBUG_LOG}
  nc -z localhost 1527
  result=$?
  echo "$(date): Port check result: $result" >> ${DEBUG_LOG}
  return $result
}

# Function to check if Derby server can handle SQL queries
check_derby_ready() {
  SQL_FILE="/app/check_health.sql"
  RESULT_FILE="/app/check_health_result.log"

  echo "$(date): Creating SQL file..." >> ${DEBUG_LOG}
  echo "CONNECT 'jdbc:derby://localhost:1527/${DB_NAME};user=admin;password=admin_password';" > $SQL_FILE
  echo "VALUES SYSCS_UTIL.SYSCS_GET_DATABASE_PROPERTY('derby.user.${DB_USER}');" >> $SQL_FILE

  echo "$(date): Attempting to connect to Derby and run the SQL query..." >> ${DEBUG_LOG}
  java -cp ${DERBY_LIB}/derbytools.jar org.apache.derby.tools.ij $SQL_FILE > $RESULT_FILE 2>> ${DEBUG_LOG}

  result=$?
  echo "$(date): Java execution result: $result" >> ${DEBUG_LOG}

  if [ $result -ne 0 ]; then
    echo "$(date): Java execution failed. Checking Java version..." >> ${DEBUG_LOG}
    java -version 2>> ${DEBUG_LOG}
    return $result
  fi

  # Check for specific error messages in the SQL query result
  if grep -q "ERROR" $RESULT_FILE || grep -q "IJ ERROR" $RESULT_FILE; then
    echo "$(date): SQL query failed with errors." >> ${DEBUG_LOG}
    return 1
  fi

  echo "$(date): SQL query result:" >> ${DEBUG_LOG}
  cat $RESULT_FILE >> ${DEBUG_LOG}

  # Check if the user property returns NULL, indicating the user does not exist
  if grep -q "NULL" $RESULT_FILE; then
    echo "$(date): User ${DB_USER} does not exist." >> ${DEBUG_LOG}
    return 1
  fi

  return 0
}

echo "$(date): Checking if Derby is healthy..." >> ${DEBUG_LOG}
if check_port && check_derby_ready; then
  echo "$(date): Health check passed." >> ${DEBUG_LOG}
  exit 0
else
  echo "$(date): Health check failed." >> ${DEBUG_LOG}
  exit 1
fi
